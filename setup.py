from setuptools import setup

setup(name='city_cat',
      version='0.1.0',
      packages=[''],
      package_dir={'': 'src'},
      scripts=['scripts/play-bot.py'],
      install_requires=['numpy', 'pandas', 'torch'])

