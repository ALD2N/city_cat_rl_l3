from city import City
from botRandomProf import BotRandom
from botExpertProf import BotExpert
from botMonteCarloProf import BotMonteCarlo
from botCrossEntropyProf import BotCrossEntropy
from botEpsilonGreedy import BotEpsilonGreedy
import os
import time
import numpy as np


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def run_n_games(bot_name: str, nb_runs: int = 10, city_width: int = 10, city_height: int = 5, cat_vitality: int = 7,
                verbose: bool = False):
    scores: int = 0
    city = City(city_width, city_height, cat_vitality)
    bot_to_call = globals()[bot_name]
    bot = bot_to_call(city)
    bot.learn(verbose)
    for iteration in range(nb_runs):
        city.reset()
        while not city.done and city.score < 100:
            move = bot.gen_move()
            if verbose:
                cls()
                print(city)
                print(move)
            city.step(move)
            if verbose:
                time.sleep(0.0001)
        scores += city.score
        print("Iteration: ", iteration, " algo ", bot_name, " -- ", city.score)
    return scores / nb_runs


def run_all_algos(nb_runs: int = 5):
    bots: np.List = ['BotRandom', 'BotExpert', 'BotMonteCarlo', 'BotEpsilonGreedy']
    scores: np.List = []
    for bot in bots:
        score = run_n_games(bot, nb_runs=nb_runs, verbose=False)
        scores.append(score)
    for i in range(len(bots)):
        print(bots[i], " scores ", scores[i])


def run_one_algo(bot: str, nb_runs: int = 30):
    score = run_n_games(bot, nb_runs=nb_runs, verbose=True)
    print(score)


def main():
    #run_all_algos()
    print(City(10, 5, 7).get_copy().get_information())
    #run_one_algo("BotEpsilonGreedy", 5)


if __name__ == '__main__':
    main()
