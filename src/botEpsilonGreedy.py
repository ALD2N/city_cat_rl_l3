import random
from bot import Bot
from city import City
from botExpert import BotExpert
import numpy as np
import copy


class BotEpsilonGreedy(Bot):
    def __init__(self, env: City):
        super().__init__(env)
        self.rng = random.Random()

    def learn(self, verbose: bool = False):
        pass

    def gen_move(self):
        listeScore = []
        listeAction = self.env.actions
        nb_simulations = 100
        epsilon = 0.1
        for action in listeAction:
            score = 0
            for i in range(0, nb_simulations):
                tempEnv = copy.deepcopy(self.env)
                tempEnv.step(action)
                while not tempEnv.done:

                    if self.rng.random() > epsilon:
                        tempEnv.step(BotExpert(tempEnv).gen_move())
                    else:
                        tempEnv.step(self.rng.choice(tempEnv.actions))
                score += tempEnv.score
            score /= nb_simulations
            listeScore.append(score)
        return listeAction[np.argmax(listeScore)]


