import random
from bot import Bot
from city import City
import numpy as np
import copy


class BotMonteCarlo(Bot):
    def __init__(self, env: City):
        super().__init__(env)
        self.rng = random.Random()

    def learn(self, verbose: bool = False):
        pass

    def gen_move(self):
        listeScore = []
        listeAction = self.env.actions
        nbSimu = 100
        for action in listeAction:
            score = 0
            for i in range(0, nbSimu):
                tempEnv = copy.deepcopy(self.env)
                tempEnv.step(action)
                while not tempEnv.done:
                    tempEnv.step(self.rng.choice(tempEnv.actions))
                score += tempEnv.score
            score /= nbSimu
            listeScore.append(score)
        return listeAction[np.argmax(listeScore)]


