import random
from bot import Bot
from city import City
import numpy as np


class BotExpert(Bot):
    def __init__(self, env: City):
        super().__init__(env)
        self.rng = random.Random()

    def learn(self, verbose: bool = False):
        pass

    def gen_move(self):
        listeInfo = self.env.generate_observations()
        listeChoix = self.env.actions
        listePoids = [0.0] * len(listeChoix)
        if listeInfo[0]:
            listePoids[0] = 100
        if listeInfo[1]:
            listePoids[0] = -100
        if listeInfo[2]:
            listePoids[0] = -10
        if listeInfo[3]:
            if listePoids[0] == 0:
                listePoids[0] = 1
        if listeInfo[6]:
            if listePoids[0] == 0 or listePoids[0] == 1 and not listeInfo[4]:
                listePoids[0] = 1
        if listeInfo[9]:
            listePoids[1] = 100
        if listeInfo[10]:
            listePoids[1] = -100
        if listeInfo[12]:
            listePoids[2] = 100
        if listeInfo[13]:
            listePoids[2] = -100
        return listeChoix[np.argmax(listePoids)]
