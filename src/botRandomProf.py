import random
from bot import Bot
from city import City
import numpy as np

class BotRandom(Bot):
    def __init__(self, env: City):
        super().__init__(env)
        self.rng = random.Random()

    def learn(self, verbose: bool = False):
        pass

    def gen_move(self):
        # TODO
        return np.random.choice(self.env.actions)
