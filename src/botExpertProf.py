import numpy as np
from bot import Bot
from city import City


class BotExpert(Bot):
    def __init__(self, env: City):
        super().__init__(env)

    def learn(self, verbose: bool = False):
        pass

    def gen_move(self):
        obs: np.list = self.env.observations
        if obs[0]:  # front is food
            return 'no'
        if obs[9]:  # left is food
            return 'left'
        if obs[12]:  # right is food
            return 'right'
        if not obs[1] and not obs[2]:
            return 'no'
        if not obs[10] and not obs[11]:
            return 'left'
        if not obs[13] and not obs[14]:
            return 'right'
        return np.random.choice(self.env.actions)